package com.algafood.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.algafood.dto.ProductDTO;
import com.algafood.entities.Product;
import com.algafood.form.ProductForm;
import com.algafood.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private EntityManager entityManager;

	public ProductDTO findById(Long id) {
		Product product = productRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Product not found"));

		return modelMapper.map(product, ProductDTO.class);
	}

	public List<ProductDTO> listProducts() {
		return productRepository.findAll().stream().map(product -> modelMapper.map(product, ProductDTO.class))
				.collect(Collectors.toList());
	}

	public ProductDTO createProduct(ProductForm form) {
		ProductDTO productDTO = modelMapper.map(productRepository.save(modelMapper.map(form, Product.class)),
				ProductDTO.class);
		return productDTO;
	}

	public ProductDTO ProductResult(Long id) {
		Product product = productRepository.getById(id);
		return modelMapper.map(product, ProductDTO.class);
	}

	public ResponseEntity<Object> delete(Long id) {
		Optional<Product> product = productRepository.findById(id);
		return product.map(prod -> {
			productRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}

	public ResponseEntity<Product> updateProduct(Long id, ProductForm form) {
		return productRepository.findById(id)
				.map(prd -> {
		               prd.setName(form.getName());
		               prd.setDescription(form.getDescription());
		               prd.setPrice(form.getPrice());
		               productRepository.save(prd);
		               return ResponseEntity.ok().body(prd);
		           }).orElse(ResponseEntity.notFound().build());
		}

	public List<ProductDTO> listProductSearch(String q, Double min_price, Double max_price) {
		List<ProductDTO> prd = productRepository.searchProduct(entityManager, q, min_price, max_price).stream()
				.map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
		;
		return prd;
	}

}