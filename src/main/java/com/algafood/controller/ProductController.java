package com.algafood.controller;

import java.net.URISyntaxException;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.algafood.dto.ProductDTO;
import com.algafood.entities.Product;
import com.algafood.form.ProductForm;
import com.algafood.service.ProductService;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/{id}")
	public ResponseEntity<ProductDTO> findById(@PathVariable Long id) {
		return new ResponseEntity<>(productService.findById(id), HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<ProductDTO>> listProducts() throws NotFoundException {
		return ResponseEntity.ok(productService.listProducts());
	}

	@GetMapping("/search")
	public ResponseEntity<List<ProductDTO>> listProductSearch(
			@RequestParam(value = "q", required = false, defaultValue = "") String q,
			@RequestParam(value = "min_price", required = false) Double min_price,
			@RequestParam(value = "max_price", required = false) Double max_price) throws NotFoundException {
		return ResponseEntity.ok(productService.listProductSearch(q, min_price, max_price));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {

		return productService.delete(id);
	}

	@PostMapping
	public ResponseEntity<ProductDTO> createProduct(@RequestBody @Valid ProductForm form)
			throws NotFoundException, URISyntaxException {
		ProductDTO productDTO = productService.createProduct(form);
		return new ResponseEntity<>(productDTO, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<Product> updateProduct(@PathVariable Long id, @Valid @RequestBody  ProductForm form) {
		return  productService.updateProduct(id, form);
	}
}
