package com.algafood.repository;

import java.text.MessageFormat;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.algafood.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	default List<Product> searchProduct(EntityManager entityManager, String q, Double min, Double max) {
		var cb = entityManager.getCriteriaBuilder();
		var cq = cb.createQuery(Product.class);
		var root = cq.from(Product.class);
		Predicate pdtName = null;
		Predicate pdtDescription = null;
		if (q != null) {
			pdtName = cb.like(cb.lower(root.get("name")), "%" + q.toLowerCase() + "%");
			pdtDescription = cb.like(root.get("description"), "%" + q.toLowerCase() + "%");
		}
		cq.where(cb.or(pdtName, pdtDescription));
		Predicate pdtMinPrice = min != null ? cb.greaterThan(root.get("price"), min) : null;
		Predicate pdtMaxPrice = max != null ? cb.lessThan(root.get("price"), max) : null;
		if (pdtMinPrice != null && pdtMaxPrice != null) {
			cq.where(cb.and(pdtMinPrice, pdtMaxPrice));
		} else if (pdtMinPrice != null) {
			cq.where(pdtMinPrice);
		} else if (pdtMaxPrice != null) {
			cq.where(pdtMaxPrice);
		}
		return entityManager.createQuery(cq).getResultList();
	}

}
