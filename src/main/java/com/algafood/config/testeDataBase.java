package com.algafood.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import com.algafood.entities.Product;
import com.algafood.repository.ProductRepository;

@Configuration
public class testeDataBase implements CommandLineRunner{

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public void run(String... args) throws Exception {
		Product p1 = new Product(null,"tv","televisor philips",999.99);
		Product p2 = new Product(null,"smartphone","smartphone lenovo",952.99);
		Product p3 = new Product(null,"geladeira","geladeira brastemp",2099.99);
		productRepository.saveAll(Arrays.asList(p1,p2,p3));
		
	}

}
