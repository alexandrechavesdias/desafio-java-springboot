## Instruções
Para utilizar o serviço basta clonar e iniciar, o banco de dados para fins de demonstração foi utilizado H2,
que esta embarcado na aplicação e populando assim que inicia o serviço.


## Documentação dos endpoints
https://desafio-java-springboot/swagger-ui.html#/
## Arquitetura
Foi sparada as responsabilidades em camadas, sendo as mesmas:
- SERVICE: Compreendendo a lógica de negócio.
- CONTROLLER: Para comunicação externa.
- REPOSITORY: Para recuperação e persistência dos dados.
- ENTITY: Para as classes de modelo.
- DTOs e FORM: Para trafegar e modelar as respostas.

## Observações
- Para conversão dos DTOs optei por utilizar a lib MODELMAPPER, que abstrai essa lógica deixando o código mais legível.
- Para as classes da camada de modelo optei por utillizar  a lib LOMBOK, neste contexto não vi problemas em abrir mão do controle da geração dos Getters e Setters.

## Ferammentas
- Para o desenvolvimento: Java 11, Spring Framework e banco de dados em memória H2 populando o banco quando é iniciada a aplicação.
- Documentação da API utilizando Swagger